openapi: 3.0.3
info:
  description: API dla biblioteki
  version: 1.0.0
  title: Mediateka API
  contact:
    name: Maciej Kubiak
    email: s4590@pjwstk.edu.pl
servers:
  - description: "Środowisko lokalne"
    url: "http://localhost:8080"
tags:
  - name: copy
  - name: user
  - name: book
  - name: borrowing
  - name: token
paths:
  '/token':
    description: "Obsługa uwierzytelniania"
    post:
      operationId: login
      description: "Logowanie do systemu"
      tags:
        - token
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginCommand'
      responses:
        201:
          description: 'Udane uwierzytelnienie'
          content:
            application/json:
              schema:
                description: "Token w postaci JWT potrzebny do uwierzytelnienia i autoryzacji w większości końcówek"
                type: string
        401:
          description: 'Nieudane uwierzytelnienie'
    get:
      operationId: refreshToken
      description: "Odświeżanie tokena"
      security:
        - bearerAuth: [ USER, LIBRARIAN, ADMIN ]
      tags:
        - token
      responses:
        200:
          description: 'Zwraca token o przedłużonej ważności'
          content:
            application/json:
              schema:
                type: string
  '/books':
    description: "Api dotyczące książek"
    post:
      operationId: createBook
      description: "Utworzenie nowej książki"
      security:
        - bearerAuth: [ LIBRARIAN, ADMIN ]
      tags:
        - book
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateBookCommand'
      responses:
        201:
          description: 'Identyfikator nowej książki'
          content:
            application/json:
              schema:
                $ref: 'ids.yaml#/BookId'
    get:
      operationId: getBooks
      description: 'Pobranie listy książek'
      tags:
        - book
      parameters:
        - name: page
          in: query
          required: false
          schema:
            $ref: 'primitives.yaml#/Integer'
        - name: pageSize
          in: query
          required: false
          schema:
            $ref: 'primitives.yaml#/Integer'
        - name: searchPhrase
          in: query
          required: false
          description: "Szukanie po części tytułu lub autora"
          schema:
            type: string
      responses:
        200:
          description: "Lista książek"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/BooksList'

  '/borrowing':
    description: "Api obsługujące wypożyczenia"
    post:
      security:
        - bearerAuth: [ LIBRARIAN, ADMIN ]
      operationId: borrowCopy
      tags:
        - borrowing
      description: "Utworzenie nowego wypożyczenia"
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateBorrowingCommand'
      responses:
        201:
          description: "Udane wypożyczenie"
          content:
            application/json:
              schema:
                $ref: 'ids.yaml#/BorrowingId'
        409:
          description: "Egzemplarz jest już wypożyczony"
    put:
      security:
        - bearerAuth: [ LIBRARIAN, ADMIN ]
      operationId: returnCopy
      tags:
        - borrowing
      description: "Zwrot książki"
      parameters:
        - name: copyInventoryNumber
          in: query
          schema:
            type: string
          required: true
      responses:
        204:
          description: "Udany zwrot"
        409:
          description: "Egzemplarz aktualnie nie jest w wypożyczeniu"

  '/copies':
    post:
      security:
        - bearerAuth: [ LIBRARIAN, ADMIN ]
      operationId: createCopy
      tags:
        - copy
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateCopyCommand'
      responses:
        201:
          description: "Tworzy nowy egzemplarz danego tytułu"
          content:
            application/json:
              schema:
                $ref: "ids.yaml#/CopyId"
  '/copies/{inventoryNumber}':
    get:
      operationId: getCopyByInventoryNumber
      tags:
        - copy
      parameters:
        - name: inventoryNumber
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "Dane kopii"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Copy'
  '/users':
    post:
      security:
        - bearerAuth: [ LIBRARIAN, ADMIN ]
      operationId: createUser
      tags:
        - user
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/CreateUserCommand'
      responses:
        201:
          description: 'creates new user'
          content:
            application/json:
              schema:
                $ref: "ids.yaml#/UserId"
  '/users/{userCardNumber}':
    get:
      security:
        - bearerAuth: [ USER, LIBRARIAN, ADMIN ]
      operationId: getUser
      tags:
        - user
      parameters:
        - name: userCardNumber
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "Dane użytkownika"
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
  '/users/{userCardNumber}/borrowings':
    get:
      security:
        - bearerAuth: [ USER, LIBRARIAN, ADMIN ]
      operationId: getUserBorrowings
      tags:
        - user
      parameters:
        - name: userCardNumber
          in: path
          schema:
            type: string
          required: true
      responses:
        200:
          description: "Aktualne wypożyczenia użytkownika"
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Borrowing'

components:
  schemas:
    BooksList:
      type: object
      required:
        - page
        - pageSize
        - totalSize
        - totalPages
        - data
      properties:
        page:
          $ref: 'primitives.yaml#/Integer'
        pageSize:
          $ref: 'primitives.yaml#/Integer'
        totalSize:
          $ref: 'primitives.yaml#/Integer'
        totalPages:
          $ref: 'primitives.yaml#/Integer'
        data:
          type: array
          items:
            $ref: '#/components/schemas/Book'
    Book:
      type: object
      required:
        - id
        - title
        - author
        - isbn
        - publicationDate
        - pages
        - numberOfCopies
        - numberOfAvailableCopies
        - availableCopyIds
      properties:
        id:
          $ref: 'ids.yaml#/BookId'
        title:
          type: string
        author:
          type: string
        isbn:
          type: string
        publicationDate:
          type: string
          format: date
        pages:
          $ref: 'primitives.yaml#/Integer'
        numberOfCopies:
          $ref: 'primitives.yaml#/Integer'
        numberOfAvailableCopies:
          $ref: 'primitives.yaml#/Integer'
        availableCopyInventoryNumbers:
          type: array
          items:
            type: string

    LoginCommand:
      type: object
      properties:
        username:
          type: string
        password:
          type: string
      required:
        - username
        - password
    CreateBookCommand:
      type: object
      properties:
        title:
          type: string
        author:
          type: string
        isbn:
          type: string
        publicationDate:
          type: string
          format: date
        pages:
          $ref: 'primitives.yaml#/Integer'

    CreateUserCommand:
      type: object
      required:
        - firstName
        - lastName
        - street
        - city
        - zipCode
        - password
        - role
        - cardNumber
      properties:
        firstName:
          type: string
        lastName:
          type: string
        street:
          type: string
        city:
          type: string
        zipCode:
          type: string
        email:
          type: string
        phone:
          type: string
        password:
          type: string
        role:
          $ref: '#/components/schemas/UserRole'
        cardNumber:
          type: string
    User:
      type: object
      properties:
        id:
          $ref: "ids.yaml#/UserId"
        firstName:
          type: string
        lastName:
          type: string
        role:
          $ref: '#/components/schemas/UserRole'

    CreateCopyCommand:
      type: object
      required:
        - inventoryNumber
        - bookId
      properties:
        inventoryNumber:
          type: string
        bookId:
          $ref: 'ids.yaml#/BookId'
    Copy:
      type: object
      required:
        - copyId
      properties:
        copyId:
          $ref: 'ids.yaml#/CopyId'

    CreateBorrowingCommand:
      type: object
      required:
        - copyId
        - userId
      properties:
        copyId:
          $ref: 'ids.yaml#/CopyId'
        userId:
          $ref: 'ids.yaml#/UserId'
    UserRole:
      type: string
      enum:
        - USER
        - LIBRARIAN
        - ADMIN
    Borrowing:
      type:
        object
      required:
        - title
        - author
        - inventoryNumber
        - borrowingDate
      properties:
        title:
          type: string
        author:
          type: string
        inventoryNumber:
          type: string
        borrowingDate:
          type: string
          format: date
  securitySchemes:
    bearerAuth:
      type: http
      scheme: bearer
      bearerFormat: JWT
